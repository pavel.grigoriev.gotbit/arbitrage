// SPDX-License-Identifier: UNLICENSED
pragma solidity >= 0.8.0;

import "@forge-std/Script.sol";
import "../src/Token.sol";
import "../src/Arbitrage.sol";
import "@uniswap/interfaces/IUniswapV3Factory.sol";
import "@uniswap/interfaces/IUniswapV3Pool.sol";

contract DeployScript is Script {
    function setUp() public {}

    function run() public {
        vm.startBroadcast();

        Token firstToken = new Token("First Token", "FRST");
        Token secondToken = new Token("Second Token", "SCND");

        console.log("First Token ", address(firstToken));
        console.log("Second Token ", address(secondToken));

        firstToken.mint(msg.sender, 1_000_000_000);
        secondToken.mint(msg.sender, 1_000_000_000);

        address uniswapV3FactoryAddress = 0x1F98431c8aD98523631AE4a59f267346ea31F984;
        IUniswapV3Factory factory = IUniswapV3Factory(uniswapV3FactoryAddress);

        uint24 poolFee500 = 500; // 0.05%
        uint24 poolFee3000 = 3000; // 0.3%

        address pool500Address = factory.createPool(address(firstToken), address(secondToken), poolFee500);
        address pool3000Address = factory.createPool(address(firstToken), address(secondToken), poolFee3000);

        console.log("Pool 500 ", pool500Address);
        console.log("Pool 3000 ", pool3000Address);

        vm.stopBroadcast();
    }
}
