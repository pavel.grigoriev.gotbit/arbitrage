// SPDX-License-Identifier: UNLICENSED
pragma solidity >= 0.8.0;

import "@forge-std/Script.sol";
import "../src/Token.sol";
import "../src/Arbitrage.sol";
import "../src/Flashloan.sol";
import "@uniswap/interfaces/IUniswapV3Factory.sol";
import "@uniswap/interfaces/IUniswapV3Pool.sol";

contract ArbitrageScript is Script {
    function run() public {
        vm.startBroadcast();

        address token1 = 0x12527F7f620ba1385ec69360F888d6EbDd96efc9;
        address token2 = 0x6645284B4732a9E4e7Ae14e72517E94f120fedDB;
        address pool500 = 0x5746369FeA9E86D2D2bD31B55aFd339457c2661E;
        address pool3000 = 0xFefB138b61a3FDD315cf41af076F8387A3c366Eb;

        Flashloan creditor = new Flashloan(token1);
        Token(token1).mint(address(creditor), 1_000_000_000);

        Arbitrage arb = new Arbitrage(token1, token2, pool500, pool3000, 0xE592427A0AEce92De3Edee1F18E0157C05861564, address(creditor));
        console.log("Arbitrage Contract ", address(arb));

        uint256 old = Token(token1).balanceOf(msg.sender);
        console.log("User balance in token1 before Arbitrage ", old);

        arb.action(10_000_000);

        uint256 next = Token(token1).balanceOf(msg.sender);
        console.log("User balance in token1 after Arbitrage ", next);
        console.log("User gained profit ", next - old);

        vm.stopBroadcast();
    }
}
