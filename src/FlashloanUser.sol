// SPDX-License-Identifier: UNLICENSED
pragma solidity >= 0.8.0;

interface FlashloanUser {
    function executeOperation(uint256) external;
}