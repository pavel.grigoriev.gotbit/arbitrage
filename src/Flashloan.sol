// SPDX-License-Identifier: UNLICENSED
pragma solidity >= 0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "./FlashloanUser.sol";

contract Flashloan is ReentrancyGuard {
    IERC20 public immutable token;

    uint256 public borrowRate = 5;

    uint256 public constant DENOMINATOR = 100;

    function flashloan(uint256 amount, address user) external nonReentrant {
        require(token.balanceOf(address(this)) >= amount, "LOW FUNDS");
        token.transfer(address(user), amount);

        FlashloanUser(user).executeOperation(amount);

        token.transferFrom(address(user), address(this), amount * (DENOMINATOR + borrowRate) / DENOMINATOR);
    }

    constructor(address token_) {
        token = IERC20(token_);
    }
}