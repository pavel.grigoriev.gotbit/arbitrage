// SPDX-License-Identifier: UNLICENSED
pragma solidity >= 0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@uniswap/interfaces/IUniswapV3Factory.sol";
import "@uniswap/interfaces/IUniswapV3Pool.sol";
import "./Flashloan.sol";
import "./FlashloanUser.sol";
import "./Token.sol";
import "../script/external/INonfungiblePositionManager.sol";
import "../script/external/ISwapRouter.sol";
import {console} from "lib/forge-std/src/console.sol";

contract Arbitrage is FlashloanUser {
    address public owner;

    Token public firstToken;
    Token public secondToken;

    IUniswapV3Pool public pool500;
    IUniswapV3Pool public pool3000;

    ISwapRouter public router;

    Flashloan public creditor;

    constructor(address firstToken_, address secondToken_, address pool500_, address pool3000_, address router_, address creditor_) {
        owner = msg.sender;

        firstToken = Token(firstToken_);
        secondToken = Token(secondToken_);

        pool500 = IUniswapV3Pool(pool500_);
        pool3000 = IUniswapV3Pool(pool3000_);

        router = ISwapRouter(router_);
        creditor = Flashloan(creditor_);
    }

    function executeOperation(uint256 flashloanAmount) external virtual override {
        console.log("amount of token1", flashloanAmount);
        // here we already have funds
        // Trade in the first pool 500
        ISwapRouter.ExactInputSingleParams memory params = ISwapRouter.ExactInputSingleParams({
            tokenIn: address(firstToken),
            tokenOut: address(secondToken),
            fee: 3000,
            recipient: address(this),
            deadline: block.timestamp + 1,
            amountIn: flashloanAmount, // flash loan the amount of first token
            amountOutMinimum: 0,
            sqrtPriceLimitX96: 0
        });

        firstToken.increaseAllowance(address(router), flashloanAmount);
        // swap token 1 for token 2
        uint256 amountOut = router.exactInputSingle(params);

        console.log("first amount out in token2", amountOut);

        params = ISwapRouter.ExactInputSingleParams({
            tokenIn: address(secondToken),
            tokenOut: address(firstToken),
            fee: 500,
            recipient: address(this),
            deadline: block.timestamp + 1,
            amountIn: amountOut, // the received amount of token 2
            amountOutMinimum: 0,
            sqrtPriceLimitX96: 0
        });

        secondToken.increaseAllowance(address(router), amountOut);
        // trade token 2 back to token 1 and get some profit
        uint256 amountOutFinal = router.exactInputSingle(params);

        console.log("second amount out in token1", amountOutFinal);

        uint256 returnAmount = flashloanAmount * (creditor.borrowRate() + creditor.DENOMINATOR()) / creditor.DENOMINATOR();
        console.log("return amount", returnAmount);

        uint256 profit = amountOutFinal - returnAmount;
        console.log("profit amount", profit);
        
        firstToken.transfer(owner, profit);

        console.log("transfer profit to user");

        // leave opportunity for the protoocol to claim money back
        firstToken.increaseAllowance(address(creditor), returnAmount);
    }

    function action(uint256 desiredAmount) external {
        creditor.flashloan(desiredAmount, address(this));
    }
}