# Arbitrage



## Install
Clone project with git clone by SSH or HTTPS.

## Init Project
Create .env file and fill it with RPC and PRIVATE_KEY variables, where RPC is link to your NODE and PRIVATE_KEY is your blockchain address private key.

## Start Working
Execute "yarn" command in the root of the project to install dependencies. Than execute "yarn deploy:dry" to simulate arbitrage transactions or execute "yarn deploy:action" to make real on-chain transactions.
